# Auteurs : Kévin Cocchi / Thomas Schrive
# Groupe 3 - Binome 9
# TP2 - Exercice 2


  # Question 1
p <- seq(-5, 5, length=1000) # 1000 points également espacés entre -5 et 5
y <- dunif(p, min=0, max=2, log=FALSE) # Densité de la loi uniforme sur l'intervalle [0,2]
plot(p, y, type="l", main="Loi uniforme sur l'intervalle [0,2]")


  # Question 2
x <- runif(40, min=0, max=1.5) # Réalisation d'un échantillon de loi uniforme sur [0,1.5] de taille 40


  # Question 3
LV <- function(theta) {
  R <- dunif(x, min=0, max=theta, log=FALSE)
  sum(log(R))
}
# Tracé de la fonction log-vraisemblance pour theta de 0 à 1000
#thetas <- seq(0,1000,length=1000)
#LVapp <- lapply(thetas, LV)
#plot(thetas, LVapp, type="l", xlab="θ", ylab="LV(θ)", lwd=2)


  # Question 4
LV(1)
LV(2)


  # Question 5
EMV <- optimize(LV, interval=c(min(x),max(x)), maximum=TRUE)$maximum


  # Question 6
# Dans la mesure où on travaille sur [0, theta], E(X) = theta/2 et V(X) = theta²/12
# Ainsi, l'estimateur de la méthode des moments n'est autre que le double de la moyenne empirique
# C'est aussi la racine de 12 multiplié par la variance empirique

m <- (1/length(x))*sum(x) # Moyenne empirique
v <- sum((x-m)^2)*(1/length(x)) # Variance empirique

MDM <- 2*m # Estimateur via l'espérance
MDMv <- sqrt(12*v) # Estimateur via la variance


  # Question 7
x <- runif(10000, min=0, max=1.7) # Réalisation d'un échantillon de loi uniforme sur [0,1.5] de taille 10000

EMV <- optimize(LV, interval=c(min(x),max(x)), maximum=TRUE)$maximum

m <- (1/length(x))*sum(x) # Moyenne empirique
v <- sum((x-m)^2)*(1/length(x)) # Variance empirique

MDM <- 2*m # Estimateur via l'espérance
MDMv <- sqrt(12*v) # Estimateur via la variance